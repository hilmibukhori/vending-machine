import java.util.Scanner;

public class MainVendingMachine {
    
//    Contoh Salah
//   Penggunaan access modifier public tidak menerapkan enkapsulasi dan menjadi kurang secure karena dapat diakses dimanapun
//  public static Scanner input = new Scanner(System.in);
//  public static int uang = 0, product, coins, bills, total = 0, kembalian = 0,options;
  
//    Contoh Benar
//    Penggunaan access modifier private akan lebih secure karena telah menerapkan enkapsulasi
  private static Scanner input = new Scanner(System.in);
  private static int uang = 0, product, coins, bills, total = 0, kembalian = 0,options;

  
  public static void payment() {
    System.out.println("\nPilih Koin / Uang Kertas.");
      OUTER:
      do {
          System.out.println("0. Pilih Produk");
          System.out.println("1. Masukkan Uang Koin");
          System.out.println("2. Masukkan Uang Kertas");
          options = input.nextInt();
          switch (options) {
              case 0:
                  break OUTER;
              case 1:
                    System.out.println("0. Selesai");
                    System.out.println("1. 200");
                    System.out.println("2. 500");
                    System.out.println("3. 1000");
                    
                  System.out.println("Masukkan Uang Koin");
                  do {
                      coins = input.nextInt();
                      switch (coins) {
                          case 0:
                              break;
                          case 1:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 200;   
                              
//                            Contoh Benar dengan penggunaan method setter
                              setTotal(200);
                              break;
                          case 2:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 500;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(500);
                              break;
                          case 3:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 1000;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(1000);
                              break;
                          default:
                              System.out
                                      .println("Error");
                      }
                  } while (coins != 0);
                  break;
              case 2:
                    System.out.println("0. Selesai");
                    System.out.println("1. 2000");
                    System.out.println("2. 5000");
                    System.out.println("3. 10000");
                    System.out.println("4. 20000");
                    System.out.println("5. 50000");
                    System.out.println("6. 100000");
                    
                  System.out.println("Masukkan Uang Kertas");
                  do {
                      bills = input.nextInt();
                      switch (bills) {
                          case 0:
                              break;
                          case 1:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 2000;   
                              
//                            Contoh Benar dengan penggunaan method setter
                              setTotal(2000);
                              break;
                          case 2:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 5000;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(5000);
                              break;
                          case 3:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 10000;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(10000);
                              break;
                          case 4:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 20000;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(20000);
                              break;
                          case 5:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 50000;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(50000);
                              break;
                           
                          case 6:
//                            Contoh Salah dengan set nilai langsung ke variabel
//                            total += 2000;   
                              
//                            Contoh Benar dengan penggunaan method setter                              
                              setTotal(100000);
                              break;
                              
                          default:
                              System.out
                                      .println("Error");
                      }
                  } while (bills != 0);
                  break;
              default:
                  System.out.println("Error");
                  break;
          }
      } while (options != 0);
  }

//  Contoh Benar
//  Karena telah menggunakan method setter untuk set nilai variable
    public static void setInput(Scanner input) {
        MainVendingMachine.input = input;
    }

//  Contoh Benar
//  Karena telah menggunakan method setter untuk set nilai variable
    public static void setUang(int uang) {
        MainVendingMachine.uang += uang;
    }

//  Contoh Benar
    //  Karena telah menggunakan method setter untuk set nilai variable
    public static void setProduct(int product) {
        MainVendingMachine.product = product;
    }

//  Contoh Benar
//  Karena telah menggunakan method setter untuk set nilai variable    
    public static void setCoins(int coins) {
        MainVendingMachine.coins = coins;
    }

//  Contoh Benar 
//  Karena telah menggunakan method setter untuk set nilai variable
    public static void setBills(int bills) {
        MainVendingMachine.bills = bills;
    }

    public static void setTotal(int total) {
        MainVendingMachine.total += total;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method setter untuk set nilai variable
    public static void setKembalian(int kembalian) {
        MainVendingMachine.kembalian = kembalian;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method setter untuk set nilai variable
    public static void setOptions(int options) {
        MainVendingMachine.options = options;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static Scanner getInput() {
        return input;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static int getUang() {
        return uang;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static int getProduct() {
        return product;
    }
    
//  Contoh Benar
    //  Karena telah menggunakan method setter untuk set nilai variable
    public static int getCoins() {
        return coins;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static int getBills() {
        return bills;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static int getTotal() {
        return total;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static int getKembalian() {
        return kembalian;
    }
    
//  Contoh Benar
//  Karena telah menggunakan method getter untuk get nilai variable
    public static int getOptions() {
        return options;
    }

  public static void purchase() {
    System.out.println("Pilih Produk.");
    product = input.nextInt();

    switch (product) {
    case 0:
      uang += 0;
      break;
    case 1:
//  Contoh Salah dengan set nilai langsung ke variabel
//  total += 55000;   
                              
//  Contoh Benar dengan penggunaan method setter
        setUang(55000);
      break;
      
    case 2:
//  Contoh Salah dengan set nilai langsung ke variabel
//  total += 100000;   
                              
//  Contoh Benar dengan penggunaan method setter
        setUang(100000);;
      break;
      
    case 3:
//  Contoh Salah dengan set nilai langsung ke variabel
//  total += 30000;   
                              
//  Contoh Benar dengan penggunaan method setter        
        setUang(30000);
      break;
      
    case 4:
//  Contoh Salah dengan set nilai langsung ke variabel
//  total += 15000;   
                              
//  Contoh Benar dengan penggunaan method setter        
        setUang(15000);
      break;
      
    case 5:
//  Contoh Salah dengan set nilai langsung ke variabel
//  total += 55000;   
                              
//  Contoh Benar dengan penggunaan method setter        
        setUang(55000);
      break;
      
    default:
      System.out.println("ERROR PRODUK");
    }
  }

  public static String calCoins(int kembalian) {
    String kembalianString = "";
    int seribuan, remSeribuan=0;
    int limaratusan, remLimaratusan=0;
    int duaratusan, remDuaratusan=0;
    int pennies=0;
    
//  Contoh Salah dengan get nilai langsung ke variabel
//  seribuan = kembalian/1000;   
                              
//  Contoh Benar dengan penggunaan method getter   
    seribuan = getKembalian()/1000;
    remSeribuan =getKembalian()%1000;
    kembalianString += seribuan + " 1000 an\n";

    if(remSeribuan!=0) {
      limaratusan = remSeribuan/500;
      remLimaratusan = remSeribuan%500;
      kembalianString += limaratusan + " 500 an\n";
    } 
    
    if (remLimaratusan!=0) {
      duaratusan = remLimaratusan/200;
      remDuaratusan = remLimaratusan % 200;
      kembalianString += duaratusan + " 200 an\n";
    }
    
    if (remDuaratusan!=0) {
      pennies = remDuaratusan;
      kembalianString += pennies + " 0\n";
    }
    return kembalianString;
  }
  
  public static String kembalian() {
        String kembalianDisplay="";
        if (getTotal() > getUang()) {
            setKembalian(getTotal() - getUang());
          kembalianDisplay = calCoins(getKembalian());
        }
        else {
          kembalianDisplay ="UANG KAMU KURANG";
        }
        
        return kembalianDisplay;
  }
  
  public static void display() {
    System.out.println("JUMLAH UANG : " + getUang());
    System.out.println("TOTAL UANG : " + getTotal());
    System.out.println("KEMBALIAN :\n " + getKembalian());
  }
  
  public static void main(String[] args) {
    System.out.println("0. Keluar");
    System.out.println("1. Fitbar Tiramisu Fitbar\tRp.55.000");
    System.out.println("2. Wafer Tango\t\tRp.100.000");
    System.out.println("3. Biskuit Oreo\tRp.30.000");
    System.out.println("4. Keripik Lays\t\tRp.15.000");
    System.out.println("5. Slai Olai\tRp.120.000");

    payment();
    purchase();
    kembalian();
    display();
  }
}
